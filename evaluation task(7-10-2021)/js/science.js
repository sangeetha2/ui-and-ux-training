console.log("This is my index js file");

// Initialize the news api parameters
let category = 'science';
let apiKey = '759d795b61d9454abd4159d57acd4eae'

// Grab the news container
let newsAccordion = document.getElementById('science');

// Create an ajax get request
var xhr = new XMLHttpRequest();

xhr.open('GET', `https://newsapi.org/v2/top-headlines?category=science&apiKey=759d795b61d9454abd4159d57acd4eae&country=in`);

//xhr.setRequestHeader('Content-Type', 'application/json');
// alert("1");
// What to do when response is ready

xhr.onload = function () {

    // alert("test.response-"+this.status);
    if (this.status === 200) {

        
        // alert(this.responseText);
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `<div style="padding-top:20px;  margin-top:3%; width:90%; ">
            <img style='float:left; width:300px;' src="${element['urlToImage']}"> 
            <h3>${element['title']}</h3>
            <p>${element['description']}<p>
            <a href="${element['url']}" target='_blank'>Read more </a> <hr></div>`;


            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()


