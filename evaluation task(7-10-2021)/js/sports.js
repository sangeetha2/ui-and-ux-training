console.log("This is my index js file");

// Initialize the news api parameters
let category = 'sports';
let apiKey = '759d795b61d9454abd4159d57acd4eae'

// Grab the news container
let newsAccordion = document.getElementById('sports');

// Create an ajax get request
var xhr = new XMLHttpRequest();

xhr.open('GET', `https://newsapi.org/v2/top-headlines?category=sports&apiKey=759d795b61d9454abd4159d57acd4eae&country=in`);

//xhr.setRequestHeader('Content-Type', 'application/json');
// alert("1");
// What to do when response is ready

xhr.onload = function () {

    // alert("test.response-"+this.status);
    if (this.status === 200) {

        
        // alert(this.responseText);
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `<div class="card" style="width: 22rem; margin-top:3%; background-color: #d1cece">
            <div class="card-body">
              <h5 class="card-title" id="sport_title">${element['title']}</h5>
              <img src="${element['urlToImage']} id="sport_image" class="img-fluid" alt="...">
              
              <a href="${element['url']}" class="btn btn-secondary">Read more</a>
            </div>
          </div>`;


            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()


